#!/usr/bin/env python3

import requests
import json
from shapely.geometry import shape, Point

with open('Iran-Mrz.geojson') as f: iran = json.load(f)

def is_in_iran(lon, lat):
    for feature in iran['features']:
        if shape(feature['geometry']).contains(Point(lon, lat)):
            return True
    return False

bboxes = ["43.72021,36.1576631,49.4438632,40.0257084",
"49.3012647,36.1576631,55.0249179,40.0257084",
"54.904292,35.391115,60.6279452,39.298373",
"44.8188428,32.2706973,50.542496,36.3308229",
"50.3120069,32.3635429,56.0356601,36.4192795",
"55.9150342,32.7709304,61.6386874,36.8072918",
"47.235835,29.0199631,52.9594882,33.227416",
"52.6411084,28.5770985,58.3647617,32.8036526",
"56.420405,28.557801,62.1440585,32.7851819",
"49.1474341,26.3941584,54.8710873,30.7113829",
"54.7641799,24.2993399,57.7578254,28.698001",
"57.7085049,24.2993499,60.7021504,28.6980106",
"60.5759365,24.2492749,63.569582,28.6498155"
]

result = []
for bbox in bboxes:
    query = requests.get('https://www.openstreetmap.org/api/0.6/notes.json?bbox=' + bbox).json()
    for feature in query['features']:
        if any('onosm' in comment['text'] for comment in feature['properties']['comments']):
            if is_in_iran(*feature['geometry']['coordinates']):
                result.append(feature)

result = {"type": "FeatureCollection", "features": result}

with open("notes.geojson", 'w') as f:
    json.dump(result, f, ensure_ascii=False)
